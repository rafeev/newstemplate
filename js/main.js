$(document).ready(function() {

	$('#scrollbarY').tinyscrollbar(); 

	$("#carusel").owlCarousel({
		navigation : true,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		pagination: false,
		navigationText : [" "," "],
	});

	$("#people").owlCarousel({
		navigation : true,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		pagination: false,
		navigationText : [" "," "],
	});

	$("#foto").owlCarousel({
		navigation : true,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		pagination: false,
		navigationText : [" "," "],
	});

	$('.baloon').hover(function(){
		var text = $(this).attr('data-text');
		$(this).append('<div class="baloon__item">' + text + '</div>')
		console.log(text);
	});

	$('.baloon').mouseout(function() {
		$(this).empty();
	});

});